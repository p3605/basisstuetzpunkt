# Basisstuetzpunkt

macht überwiegend Sinn im deutschsprachigen DACH-Raum, deshalb auch in deutsch beschrieben.

## Description
Ihr habt einen Starlink Internetzugang, notstromversorgt und mit einem Ethernet-Adapter? Dann habt ihr Internetzugang auch wenn der Rest der Nachbarschaft weder Telefon noch Mobilfunkt hat.
Mit dem optionalen Ethernet-Adapter könnt ihr einen Notfall-Treffpunkt oder Basis-Stützpunkt für eure Nachbarschaft zur Verfügung stellen.

Im Sommer braucht die Starlink Dish und das Modem etwas weniger als 40 Watt. Dazu der RaspberryPi mit etwa 5 Watt. Das alles könnt ihr an einer kleineren Powerstation absolut mobil und über einen längeren Zeitraum betreiben, selbst wenn eure Nachbarn noch das Handy an der Powerstation aufladen.


Im Prinzip macht der RaspberryPi einen Access-Point an dem sich eure Nachbarn einloggen. Es gibt eine Startseite auf der die wichtigsten Links zu finden sind. Die Anfragen der im WLAN eingeloggten Nachbarn werden über den Ethernet-Port an den Starlink geroutet.
So seid Ihr Anlaufpunkt und Informationspunkt.

**Das fertige Image für einen RaspberryPi 4 könnt ihr hier downloaden:**

[Das RaspberryPi Image](https://fuerles.de/wp-content/uploads/2024/01/notfalltrefpkt_shrink.img.xz "Image zum Download")


## Visuals
Als Netzplan sieht das so aus:
![Netzplan](/media/Selbsthilfebasis_Schaltbild.png "das braucht ihr")



Ihr erstellt so eine Info-Seite für eure Selbsthilfebasis zum einloggen:
![Hilfe zum einloggen](/media/Selbsthilfebasis_Beispiel_Layout.png "QR-Codes zum scannen")


eure Nachbarn mit Handy landen auf dieser Seite:
![Handy-Ansicht](/media/Selbsthilfebasis_HTML_responsive_mobile.png "die Startseite mit Links mobil")


eure Nachbarn mit Tablet oder Laptop landen auf dieser Seite:
![Laptop-Ansicht](/media/Selbsthilfebasis_Laptop.png "die Startseite mit Links Laptop")


## Modification
Das Image passt für einen 4er RaspberryPi mit integriertem WLAN und Ethernetbuchse. Wollt ihr einen 3er RaspberryPi verwenden, nehmt einen WLAN-Adapter für USB. Der ist dann meist WLAN1 statt WLAN0 und ihr müsst die nachfolgenden Dateien anpassen.
Der interne WLan Adapter hatte bei mir zu viele Probleme gemacht und einen stabilen Aufbau habe ich erst mit dem extra USB WiFi Adapter erzielt. Dieser zusätzliche WiFi-Adapter ist dann meistens der WLAN1 Adapter anstelle dem internen WLAN0 Adapter.

Damit alles wie am Pi4 funktioniert muss beim Einsatz eines extra WiFi-Adapters in den folgenden Dateien der Eintrag wlan0 durch wlan1 für den USB Wifi-Adapter ersetzt werden.

Der RaspberryPi arbeitet ohne grafische Oberfläche, der User is „pf“ (nicht pi), das Passwort lautet „raspberry“ und der Hostname ist basis:
- User: pf
- Passwort: raspberry
- Host: basis

Nachfolgend aufgeführt die 3 wichtigsten Dateien in denen ihr die technische Konfiguration vornehmt:
## Hinweis: 
Wenn der extra WiFi Adapter zum Einsatz, kommt überall das wlan0 durch wlan1 ersetzen, sofern wlan1 der Adaptername eures USB WiFi-Adapters ist. Mit dem Befehl ifconfig könnt ihr euch die WiFi Adapter anzeigen lassen.
Befehl: `ifconfig`

![Terminal-View](/media/Selbsthilfebasis_boot_headless.png "achtet auf den User pf (nicht pi)")

das sind die wichtigsten Dateien:

![hier ist alles abgelegt](/media/Selbsthilfebasis_wichtige_Dateien.png "wollt ihr Hostname, SSID und Passwort anpassen?")

IP-Bereich festlegen mit `sudo nano /etc/dhcpcd.conf`:

![IP-Bereich](/media/Selbsthilfebsis_DHCPD_conf.png "voreingestellt ist 192.168.1.xxx")

welches WLAN Interface soll gelten (WLan0/WLan1) und wie ist der DHCP-Bereich?
Hier könnt ihr natürlich deutlich mehr als die 18 (von .2 bis .20) User zulassen. 
Gebt dann statt 192.168.4.20 zum Beispiel 192.168.4.52 an. Das sind dann von .2 bis .52 50 Leases. Geht ganz am Ende der Zeile statt 25h nur 2h an, dann werden die Leases früher wieder für andere Nachbarn frei. Mit `sudo nano /etc/dnsmasq.conf`:

![DHCP Range und Interfaces](/media/Selbsthilfebasis_DNSMASQ_conf.png "WLAN-Interface anpassen und Startseite")

Ihr wollt die WLAN Zugangsdaten ändern? `sudo nano /etc/hostapd/hostapd.conf`:

![SSID und Passwort](/media/Selbsthilfebasis_HOSTAPD_conf.png "Zugangsdaten festlegen")

Mit diesem QR-Code konfiguriert ihr euer Handy fürs WLAN:
![QR-Code mit Netzwerkkonfiguration](/files/QR-Code_WLAN_Notfall-Treffpunkt.png "scannen und im WLAN einloggen")

Dieser QR-Code bringt euch auf die Startseite mit den Links:
![QR-Code zur Startseite](/files/QR-Code_Startseite_Notfall-Treffpunkt.png "scannen und ihr seid auf der Startseite")


## Installation
Spielt das vorbereitete Image auf eine neue SD-Karte für den RaspberryPi. Legt diese ein und startet den RaspberryPi. Der SSH-Zugang ist freigeschaltet (pf:raspberry) und das Image sollte sich automatisch expandiert haben.


## Usage
- Starlink-Dish mit freier Sicht zum Himmel aufbauen, 
- Ehternet-Adapter einschleifen. 
- Patch-Kabel zum 4er RaspberryPi. 
- Starlink und RaspberryPi mit Strom aus einer Powerbank versorgen. Ich: Bluetti EB55. 
- Blatt mit Zugangsdaten auslegen, 
- Nachbarschaft informieren, 
- einloggen lassen. 

Neue Links und Informationen editiere ich live in die index.html und nutze dazu den WYSIWYG Editor BlueGriffon im dual-view Modus. Dabei schreibt ihr einfach in der linken Bildhälfte und der HTML-Code entsteht parallel auf der rechten Seite:
![BlueGriffon editieren](/media/Selbsthilfebasis_BlueGriffon_editieren.png "editieren der index.html")

BlueGriffon ist für Windows und Linux erhältlich. 
`sudo apt install ./bluegriffon-3.1.Ubuntu18.04-x86_64.deb` 
![BlueGriffon Info](/media/Selbsthilfebasis_BlueGriffon_Info.png "Info zu BlueGriffon")

Den Zugriff auf die index.html auf dem RaspberryPi bekomme ich per:

`sftp://pf@Ethernet-IP-Pi/var/www/html/index.html`


## Support
for all your questions use `pf@nc-x.com`

## Roadmap
läuft erstmal, sammle weiter interessante Links

## Contributing
eure Mitarbeit und Kritik ist hochwillkommen. Besonderer Dank geht an Twitterer @Ingmar_Stapel der alles überprüft und die Dokumentation für die notwendigen Änderungen an einem 3er Pi verbessert hat.

## Authors and acknowledgment
Inspiriert von den Notfalltreffpunkten die in der Schweiz jedes Kaff hat und bei uns mit der Lupe gesucht werden müssen.

## License
MIT Lizenz.

## Project status
lauffähig, aber ohne Feinheiten.
